{config, lib, ...}:

{
  boot = {
    kernelModules = [ "kvm-intel" "tmp-rng" "tp_smapi" ];
    extraModulePackages = [ config.boot.kernelPackages.tp_smapi ];
  };
  sound.enableMediaKeys = true;
  hardware.trackpoint.enable = true;
  hardware.trackpoint.emulateWheel = true;
  security.rngd.enable = true;
}
